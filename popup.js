const MAX_ITEMS = 5;

function sorter(array) {
    return Object.keys(array).sort((a, b) => {
        return array[a] <= array[b];
    });
}

function addElements(element, array, callback) {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
    if (!array)
        return;
    for (let i = 0; i < array.length; i++) {
        if (i >= MAX_ITEMS) {
            break;
        }

        const listItem = document.createElement("li");
        listItem.textContent = callback(array[i]);
        element.appendChild(listItem);
    }
}
function getReady() {
    setInterval(() => {
    chrome.storage.sync.get('hotels', (data) => {
        let hotelElement = document.getElementById("hotels");
        console.log(data);
        addElements(hotelElement, data.hotels, (hotel) => {
            return `${hotel.name}: ${hotel.price}`;
        });
});
    }, 1000);
    
}
window.addEventListener("load", getReady);
