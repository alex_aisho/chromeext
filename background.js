let status = false;

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
    console.log(message);
    switch (message.message) {
        case 'get-status':
            sendResponse({ message: status });
            break;
        case 'set-status':
            status = Boolean(message.content);
            sendResponse({ message: status });
            break;
        case 'set-content':
            console.log(sendResponse({ message: 'OK' }));
            status = false;
        default:
            break;
    }
});