function ready() {
    chrome.runtime.sendMessage({ message: 'set-status', content: true }, (response) => {
        console.log('received user data', response);
    });
    let area = document.getElementById('ss');
    area.click();
    area.setAttribute('value', 'Побережье Азовского моря, Россия');
    area.dispatchEvent(new Event('input', {
         view: window,
         bubbles: true,
         cancelable: true
    }));

    let dates = document.querySelector("#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__dates.xp__group > div.xp__dates-inner > div:nth-child(2)");
    dates.click();
    let calendar = document.querySelector("#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__dates.xp__group > div.xp-calendar > div > div > div.bui-calendar__content > div:nth-child(1) > table").getElementsByTagName('tbody')[0];
    for (el of calendar.childNodes) {
        let mark = false;
        for (td of el.childNodes) {
            if (td.classList)
            {
            if (!td.classList.contains('bui-calendar__date--disabled') && 
                !td.classList.contains('bui-calendar__date--today') &&
                !td.classList.contains('bui-calendar__date--empty')) {
                    console.log(td);
                    td.click();
                    mark = true;
                    break;
            } 
        }
        }
        if (mark) {
            break;
        }
    }

    calendar = document.querySelector("#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__dates.xp__group > div.xp-calendar > div > div > div.bui-calendar__content > div:nth-child(2) > table").getElementsByTagName('tbody')[0];
    for (el of calendar.childNodes) {
        let mark = false;
        for (td of el.childNodes) {
            if (td.classList)
            {
            if (!td.classList.contains('bui-calendar__date--disabled') && 
                !td.classList.contains('bui-calendar__date--today') &&
                !td.classList.contains('bui-calendar__date--empty')) {
                    console.log(td);
                    td.click();
                    mark = true;
                    break;
            } 
        }
        }
        if (mark) {
            break;
        }
    }
    let submit = document.querySelector("#frm > div.xp__fieldset.js--sb-fieldset.accommodation > div.xp__button > div.sb-searchbox-submit-col.-submit-button > button");
    console.log('submit: ' + submit);
    submit.click();

};


function getData() {
    let data = document.querySelectorAll('div.sr_item_content.sr_item_content_slider_wrapper');
    console.log(data);    
    let list = [];
    for (el of data) {
        let price = (el.querySelector('div.bui-price-display__value').innerText);
        let name = (el.querySelector('span.sr-hotel__name').innerText);
        let json = {name : name, price : price};
        list.push(json);
    }
    chrome.runtime.sendMessage({message : 'set-content'});
    console.log(list);
    chrome.storage.sync.set({hotels : list}, function() {
             console.log(list);
    });
}

function getReady() {
    setTimeout(function () {
        chrome.runtime.sendMessage({ message: 'get-status' }, (response) => {
        console.log('received user data', response);
        if (response.message === false) {
            ready();
        } else {
            getData();
        }
    });
    }, 500)
    
}

 window.addEventListener("load", getReady);
 